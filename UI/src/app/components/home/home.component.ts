import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { first } from 'rxjs/operators';
import { UserAccount } from '../../models/userAccountModel';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public users: UserAccount[] = [];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getAll().pipe(first()).subscribe(users => {
      this.users = users;
    });
  }

}
