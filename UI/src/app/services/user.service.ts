import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserAccount } from '../models/userAccountModel';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private htpp: HttpClient) { }

  getAll() {
    return this.htpp.get<UserAccount[]>('/api/users');
  }
}
