export interface Config {
    signInUrl: string;
    signUpUrl: string;
}