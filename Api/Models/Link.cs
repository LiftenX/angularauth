﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Models
{
    public class Link
    {
        public string Id { get; set; }
        public string Url { get; set; }

        public int ProjectId { get; set; }
        public Project Project { get; set; }
    }
}
