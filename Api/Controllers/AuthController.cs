﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Api.Models;
using Api.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IConfiguration configuration;

        public AuthController(UserManager<AppUser> manager, IConfiguration config)
        {
            _userManager = manager;
            configuration = config;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Post([FromBody]LoginViewModel model)
        {
            IActionResult response = Unauthorized();
            if (await CheckValidUser(model.UserName, model.Password))
            {
                var jwtToken = BuildJwtToken();
                response = Ok(new { access_token = jwtToken });
            }
            else response = BadRequest(new { error = "Invalid username or password" });
            return response;
        }

        private async Task<bool> CheckValidUser(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return false;
            var userToVerify = await _userManager.FindByNameAsync(userName);

            if (userToVerify == null)
                return false;

            if (await _userManager.CheckPasswordAsync(userToVerify, password))
                return true;

            return false;
        }

        private object BuildJwtToken()
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:key"]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var jwtToken = new JwtSecurityToken(
                issuer: configuration["JWT:issuer"],
                audience: configuration["JWT:audience"],
                signingCredentials: credentials,
                expires: DateTime.Now.AddMinutes(10)
                );
            return new JwtSecurityTokenHandler().WriteToken(jwtToken);
        }
    }
}